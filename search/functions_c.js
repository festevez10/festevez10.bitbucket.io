var searchData=
[
  ['scan_189',['scan',['../classTouchPanel_1_1TouchPanel.html#a359880d28b9b1b01a8a8f40df6316c97',1,'TouchPanel.TouchPanel.scan(self)'],['../classTouchPanel_1_1TouchPanel.html#a359880d28b9b1b01a8a8f40df6316c97',1,'TouchPanel.TouchPanel.scan(self)']]],
  ['sendchar_190',['sendChar',['../SerialCommunication_8py.html#a3c53fed4795a5ace6a4567669f39bc4d',1,'SerialCommunication']]],
  ['set_5fduty_191',['set_duty',['../classLab__8__MotorDriver_1_1MotorDriver.html#a75c9ae1298996041c24d6781a699993c',1,'Lab_8_MotorDriver.MotorDriver.set_duty()'],['../classMotorDriver_1_1MotorDriver.html#a90f11354b5a2691c601423be619818ff',1,'MotorDriver.MotorDriver.set_duty()']]],
  ['set_5fposition_192',['set_position',['../classLab__8__EncoderDriver_1_1EncoderDriver.html#ac3654ba94a69a576f77c640758ad13f1',1,'Lab_8_EncoderDriver.EncoderDriver.set_position()'],['../classEncoderDriver_1_1EncoderDriver.html#a4881caae635d41ccfe54397cadf53d88',1,'EncoderDriver.EncoderDriver.set_position()']]],
  ['setdata_193',['setData',['../classmcp9808_1_1mcp9808.html#accf8eb2e83406c8fc02671ebf6b875dc',1,'mcp9808::mcp9808']]],
  ['settimer_194',['setTimer',['../lab__2__main_8py.html#ab9cff4181cb1994fd6cbb3ccf889f245',1,'lab_2_main.setTimer()'],['../ADC_8py.html#aee447f81ee5ebd429b00537da23115eb',1,'ADC.setTimer()']]],
  ['setupserialcom_195',['setupSerialCom',['../SerialCommunication_8py.html#acac2da053443070150083500e39aa957',1,'SerialCommunication']]],
  ['solveequation_196',['solveEquation',['../lab6_8py.html#af0ca710d54465e32b695d8a98cdc76a1',1,'lab6']]],
  ['solveequationclosed_197',['solveEquationClosed',['../lab6_8py.html#a1c19aff04b66a80ac9ec2b5f25879124',1,'lab6']]]
];
