/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Estevez_Project", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab0x00 Documentation: Setting up Tools for ME405", "index.html#sec_lab0", null ],
    [ "Lab0x01 Documentation: Reintroduction to Python and Finite State Machines", "index.html#sec_lab1", null ],
    [ "Lab0x02 Documentation: Think Fast!", "index.html#sec_lab2", null ],
    [ "Lab0x03 Documentation: Pushing the Right Buttons", "index.html#sec_lab3", null ],
    [ "Lab0x04 Documentation: Hot or Not?", "index.html#sec_lab4", null ],
    [ "Lab0x05 Documentation: Feeling Tipsy?", "index.html#sec_lab5", null ],
    [ "Lab0x06 Documentation: Simulation or Reality?", "index.html#sec_lab6", null ],
    [ "Lab0x07 Documentation: Feeling Touchy", "index.html#sec_lab7", null ],
    [ "Lab0x08 Documentation: Term Project Part I", "index.html#sec_lab8", null ],
    [ "Lab0x09 Documentation: Term Project Part II", "index.html#sec_lab9", null ],
    [ "Lab5 Calculations", "Lab5Calc.html", null ],
    [ "Lab6 Simmulation", "Lab6Output.html", null ],
    [ "Lab9 Term Project II", "Lab9Output.html", [
      [ "Objective", "Lab9Output.html#obj", null ],
      [ "Source Code", "Lab9Output.html#sc", null ],
      [ "Code Documentation", "Lab9Output.html#doc", null ],
      [ "Video Demenstration", "Lab9Output.html#vid", null ],
      [ "Tuned Values for Controller Gain", "Lab9Output.html#info", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ADC_8py.html",
"classLab__8__MotorDriver_1_1MotorDriver.html#ac68c92ccca8b3fd5250202ffe4cd3ec3",
"pages.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';