var namespaces_dup =
[
    [ "ADC", null, [
      [ "main", "ADC_8py.html#a05e602ba1e7ad94493b1663265a4cb8d", null ],
      [ "onButtonPress", "ADC_8py.html#a8c32f1272eb286f02f75a05b787e01a9", null ],
      [ "setTimer", "ADC_8py.html#aee447f81ee5ebd429b00537da23115eb", null ],
      [ "buttonPressed", "ADC_8py.html#a1e96359e6106b4ade3d1422e4ec18e81", null ],
      [ "timer2", "ADC_8py.html#a3c4de2464cb87292b637d6002141edef", null ]
    ] ],
    [ "constant", "namespaceconstant.html", [
      [ "CUKE", "namespaceconstant.html#ab51256d2282af9ff142262023c203854", null ],
      [ "DIME", "namespaceconstant.html#a995fc3ea1a88e21f2b56dfdc2b8eab91", null ],
      [ "DIME_IDX", "namespaceconstant.html#a3de93bde5b17a9c524801537d0e3587d", null ],
      [ "DOLLAR_IDX", "namespaceconstant.html#a3a58ffe4d8b24fe2474be069ec995d65", null ],
      [ "DR_PUPPER", "namespaceconstant.html#a54d8b3ab874d2a66d2390b115694075d", null ],
      [ "FIVE_DOLLAR", "namespaceconstant.html#a83207a66ce55fc86a37011d8b6ba2e42", null ],
      [ "FIVE_IDX", "namespaceconstant.html#a2cb6bd280a75e663450962963be35147", null ],
      [ "NICKLE", "namespaceconstant.html#a478c8c8cd4db12206c9a5d7348d0020c", null ],
      [ "NICKLE_IDX", "namespaceconstant.html#a75c4ea9c1428245a7ae380e1e0431a71", null ],
      [ "ONE_DOLLAR", "namespaceconstant.html#a705f3ebcd950b2c970750e4aad63f67b", null ],
      [ "PENNY", "namespaceconstant.html#ac1fe80775c230f8f3410cf7196e8f12f", null ],
      [ "PENNY_IDX", "namespaceconstant.html#a14a85486bd849174d10fc9d81bbaa68d", null ],
      [ "POPSI", "namespaceconstant.html#a4ef78da73de4bb9326059d6af7da5464", null ],
      [ "QUARTER", "namespaceconstant.html#a086633372b7a3326948814534ce9a050", null ],
      [ "QUARTER_IDX", "namespaceconstant.html#a8417877c3052b75dac1c2b871ef69528", null ],
      [ "SPRYTE", "namespaceconstant.html#aa30bca45cb1c165eafa9bd963d679f0a", null ],
      [ "TEN_DOLLAR", "namespaceconstant.html#a89de16311ee1c0d8e60c302cd46a81fd", null ],
      [ "TEN_IDX", "namespaceconstant.html#a9119ed8ff71229901ba4b25f06c5ae2a", null ],
      [ "TWENTIE_DOLLAR", "namespaceconstant.html#a3314f80853542a0ffb9452e61e25509e", null ],
      [ "TWENTY_IDX", "namespaceconstant.html#a1e2701e42c1344068ad04a6315c74c1c", null ]
    ] ],
    [ "Constants", null, [
      [ "BIG", "Constants_8py.html#ad4f113c8432422b4df98dbdb4c7594fc", null ],
      [ "CLK_FRQ", "Constants_8py.html#a65158546473d323c3f51283abf807d83", null ],
      [ "END", "Constants_8py.html#a99d3d2120fb63096b525e50767838d05", null ],
      [ "MILISEC", "Constants_8py.html#a4046cdbddba2e3c5c2b8a05d03b05c9b", null ],
      [ "NUCLEO_FRQ", "Constants_8py.html#a25bcd1c16183eb6445e390b3c75df7f6", null ],
      [ "START", "Constants_8py.html#a72f22d7f058ac6394d0af0b61e24d2b9", null ],
      [ "STEP", "Constants_8py.html#ac45bf7da0f57002464065dfbd5926acf", null ],
      [ "TIMER2", "Constants_8py.html#a9b1757b7577e53cdbfb192ffc5be2842", null ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ],
      [ "ed1", "EncoderDriver_8py.html#ae2cd275418684cf717cb1bf1a04085ff", null ],
      [ "ed2", "EncoderDriver_8py.html#a83738f3fa68bcfc6c05cc36c1b0185f4", null ],
      [ "pin1", "EncoderDriver_8py.html#aa95a10ef95dd168fdfeee4cb896d6fcb", null ],
      [ "pin2", "EncoderDriver_8py.html#a1af40ae18350f10e543a6cc104a49c9e", null ],
      [ "pin3", "EncoderDriver_8py.html#a7349ec9b041306e6342f34eeda557b87", null ],
      [ "pin4", "EncoderDriver_8py.html#a6bcb73b11c48356c61ff54c4c9e47899", null ],
      [ "tim4", "EncoderDriver_8py.html#a363cad8a4f2a942f08d19a18a42ffc36", null ],
      [ "tim8", "EncoderDriver_8py.html#adb072241d6fb067447561998dae68ce5", null ]
    ] ],
    [ "hw01", "namespacehw01.html", [
      [ "get_change_tuple", "namespacehw01.html#ab097bb16b9be0d47ec8fde343c3720c4", null ],
      [ "get_payment_val", "namespacehw01.html#aea5a02e57d316bc7b518fe259075f469", null ],
      [ "getChange", "namespacehw01.html#a49ae6d4fda08a680e7c899f5f1e012b6", null ],
      [ "DIME", "namespacehw01.html#a5bc81e44028be84102d5ea0d92189303", null ],
      [ "FIVE_DOLLAR", "namespacehw01.html#afb4cc427f5190e085f1fd9957c6d7d55", null ],
      [ "NICKLE", "namespacehw01.html#acdfd38c353d5d066f79fa8be2e50403c", null ],
      [ "ONE_DOLLAR", "namespacehw01.html#ab3166f2c201190f489726a4766323ad3", null ],
      [ "payment", "namespacehw01.html#aaeef8e0be94779f90b44bef33ea7314a", null ],
      [ "PENNY", "namespacehw01.html#a1d980599fe662d993a0ee161fdd949f5", null ],
      [ "price", "namespacehw01.html#a1de5b48e1b6ac35ec0c210b6b7cb83b8", null ],
      [ "QUARTER", "namespacehw01.html#aba27175275ffd0c12e63fdc565ba7e25", null ],
      [ "TEN_DOLLAR", "namespacehw01.html#a7fe06c3160d2da237ac9a2796b4783ad", null ],
      [ "TWENTIE_DOLLAR", "namespacehw01.html#a8c2046dea1d0a47c0d59eb55022d7207", null ]
    ] ],
    [ "lab6", null, [
      [ "graphMyEquation", "lab6_8py.html#a7d6cdca05582a511f3cbf036eaac462e", null ],
      [ "graphMyEquationClosed", "lab6_8py.html#a4fd786e480d0e670dcd91a5493687b80", null ],
      [ "solveEquation", "lab6_8py.html#af0ca710d54465e32b695d8a98cdc76a1", null ],
      [ "solveEquationClosed", "lab6_8py.html#a1c19aff04b66a80ac9ec2b5f25879124", null ],
      [ "b", "lab6_8py.html#a30ea588d22c9f9c5a5f95ba4998d6378", null ],
      [ "g", "lab6_8py.html#aa3970d0012b12d7243336e1142b56715", null ],
      [ "Ib", "lab6_8py.html#aff4b4a93e1635d918bd0be670d1fcdbf", null ],
      [ "Ip", "lab6_8py.html#a931c5ba57b9f3dd3bd157460817a2ccc", null ],
      [ "lp", "lab6_8py.html#a93860e4635f906c300590c09da9dfe27", null ],
      [ "lr", "lab6_8py.html#abc0679bbb5173f39f2ff14472c8d617a", null ],
      [ "mb", "lab6_8py.html#a020cea7a029fdd75d82dd9c628ee7410", null ],
      [ "mp", "lab6_8py.html#a4202290bcbdc6f7d87f40c7bccb9ca59", null ],
      [ "rb", "lab6_8py.html#ad55b810ac197259eded0f984a1d31a31", null ],
      [ "rc", "lab6_8py.html#ab6de4b280f02675c59a257a6e1ec8620", null ],
      [ "rg", "lab6_8py.html#ab6994e0aeda7d4217858e9378ca0b76f", null ],
      [ "rm", "lab6_8py.html#aefe71ca6b7fb057c120a7e17fea7dded", null ],
      [ "rp", "lab6_8py.html#ab30739acc9824ca236b215a1b5891fbe", null ],
      [ "stateVec", "lab6_8py.html#a1b26703c6054f2d6e8276c72094288a6", null ]
    ] ],
    [ "lab_2_main", null, [
      [ "beginReaction", "lab__2__main_8py.html#a8664c1c9ea3a7dc1dbaa569f9791b46f", null ],
      [ "getRandomSec", "lab__2__main_8py.html#ab63591c5254f2052dfa849128998f43e", null ],
      [ "onButtonPress", "lab__2__main_8py.html#ad3ca9075515e01051cbfe58d1eb3a5b3", null ],
      [ "setTimer", "lab__2__main_8py.html#ab9cff4181cb1994fd6cbb3ccf889f245", null ],
      [ "averageReactTime", "lab__2__main_8py.html#aeb4748762be5cfca6a974136ceb440cc", null ],
      [ "buttonCnt", "lab__2__main_8py.html#abf765f5ee4c4236a25997e7ac324a832", null ],
      [ "delayMs", "lab__2__main_8py.html#adfbfd8c369ebbdb8bc7c71d7ecd3dd33", null ],
      [ "LEDTimeOn", "lab__2__main_8py.html#aabc5dd1d0ad019a971e5785bd00044e5", null ],
      [ "myLEDPin", "lab__2__main_8py.html#a8c0bcfe7343c227fa0704f1ce1f347ac", null ],
      [ "pinC13", "lab__2__main_8py.html#af13f93062d1c4d6005000c8077df3cdc", null ],
      [ "timer2", "lab__2__main_8py.html#a69342b7a18534ac4589350e011a7b2e8", null ]
    ] ],
    [ "lab_3_main", null, [
      [ "main", "lab__3__main_8py.html#a4faecc7b428bd49f539142070175e4e7", null ],
      [ "onButtonPress", "lab__3__main_8py.html#a0c26ea502de37838d4961ac3a8ea5a20", null ],
      [ "buttonPressed", "lab__3__main_8py.html#a41089d6d86fb72801423d20302f8be46", null ]
    ] ],
    [ "lab_4_main", null, [
      [ "dataToCSV", "lab__4__main_8py.html#abd45e279559bdb8e53dc80ad43943724", null ],
      [ "main", "lab__4__main_8py.html#ac95338496f555b2fa169304ab32ee70f", null ],
      [ "MAX_TIME", "lab__4__main_8py.html#abbd9bf5a8b942fb434dac20e4e07ec33", null ],
      [ "ONE_MINUTE", "lab__4__main_8py.html#a62f8c1ca5c8e9412437847b7352ad65c", null ]
    ] ],
    [ "Lab_8_EncoderDriver", null, [
      [ "EncoderDriver", "classLab__8__EncoderDriver_1_1EncoderDriver.html", "classLab__8__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab_8_FaultDetection", null, [
      [ "callback", "Lab__8__FaultDetection_8py.html#a3a23c9c1171e664c2b35a7f7d1fcb24b", null ],
      [ "main", "Lab__8__FaultDetection_8py.html#a10668532151c6a18a0ff4e3af0dfb9b3", null ],
      [ "faultDetected", "Lab__8__FaultDetection_8py.html#acca0634c57ee3cb0f703c0ec45910bd8", null ]
    ] ],
    [ "Lab_8_main", null, [
      [ "callback", "Lab__8__main_8py.html#a26b8cb23d5ef36cb645c6914631c7e29", null ],
      [ "clear_fault", "Lab__8__main_8py.html#a548c6ae353acc8cecf04125ee5fb1060", null ],
      [ "button", "Lab__8__main_8py.html#a333a5252aba642be7d414ec28e286c1b", null ],
      [ "ed1", "Lab__8__main_8py.html#a7b416b21a5ddeb991ed6a34c2134bc7d", null ],
      [ "ed2", "Lab__8__main_8py.html#ab9509dce1bbe9c2aaec3a610bfaa2fb3", null ],
      [ "externInterupt", "Lab__8__main_8py.html#a4741130c4226c805d2f63a93698c6069", null ],
      [ "externInterupt_button", "Lab__8__main_8py.html#a24575026dd1972771e37e422193231de", null ],
      [ "faultDetected", "Lab__8__main_8py.html#a51eec5125198f1db69e141bcda75a509", null ],
      [ "i2c", "Lab__8__main_8py.html#addcbfe99fbb12ac32dbb71d19543cb2f", null ],
      [ "IN1", "Lab__8__main_8py.html#a78985175b237b6ab9b86025ad5a1f115", null ],
      [ "IN2", "Lab__8__main_8py.html#ac45e478392540aeed3132943f5ca9e08", null ],
      [ "IN3", "Lab__8__main_8py.html#a9bca422065f2c535e09344ae181deb05", null ],
      [ "IN4", "Lab__8__main_8py.html#a16fa3565e931479a4872e82ba702ed23", null ],
      [ "md", "Lab__8__main_8py.html#adcaaae55062c4b85cfee4eafa8b3a367", null ],
      [ "nFault", "Lab__8__main_8py.html#a405fa8e8b168fde9c7020957951cc22d", null ],
      [ "nSLEEP", "Lab__8__main_8py.html#a05fb69afbb761fff0ad0f15883ab7a0d", null ],
      [ "pin1", "Lab__8__main_8py.html#ac0a2cde857d4705d87a6643460b9d15c", null ],
      [ "pin2", "Lab__8__main_8py.html#ac2aab92fb10ec0b9274ab8f275730fae", null ],
      [ "pin3", "Lab__8__main_8py.html#a9d883c77ed62d14525bbfc9b4c403afd", null ],
      [ "pin4", "Lab__8__main_8py.html#aa3a187959fba847466975e87a61d5d80", null ],
      [ "tim_3", "Lab__8__main_8py.html#a5a5d3dd5fb2c2fd7e4a4465219569b9e", null ],
      [ "tim_4", "Lab__8__main_8py.html#afe98bdef190a1db624817b0991e21a9d", null ],
      [ "tim_8", "Lab__8__main_8py.html#a487053945bdfdb38ad8d915d2c942b04", null ]
    ] ],
    [ "Lab_8_MotorDriver", null, [
      [ "MotorDriver", "classLab__8__MotorDriver_1_1MotorDriver.html", "classLab__8__MotorDriver_1_1MotorDriver" ]
    ] ],
    [ "Lab_9_main", null, [
      [ "callback", "Lab__9__main_8py.html#ad8893cfc9d0ccbf1d05ccc91cf20e563", null ],
      [ "clear_fault", "Lab__9__main_8py.html#a86afb961ccff3a24546a922065669462", null ],
      [ "main", "Lab__9__main_8py.html#acc4a0ecaa537033faa447c2b4bec055c", null ],
      [ "button", "Lab__9__main_8py.html#abb046272e59caf5830f6a9d83f454328", null ],
      [ "center", "Lab__9__main_8py.html#ae85c90f42dc802beb962728110ea4891", null ],
      [ "encoder1", "Lab__9__main_8py.html#aaa0f612393219633c91be4f4c04a772c", null ],
      [ "encoder2", "Lab__9__main_8py.html#a36df7242b9bf0ebd36e5437807f4fa28", null ],
      [ "encoderPin1", "Lab__9__main_8py.html#a4ec2f00dc242ee0dab155a91ff28d88c", null ],
      [ "encoderPin2", "Lab__9__main_8py.html#a361af77ab187b5108f5a7509c3d7829b", null ],
      [ "encoderPin3", "Lab__9__main_8py.html#a603d80f5c1e19037daccf59f476374bc", null ],
      [ "encoderPin4", "Lab__9__main_8py.html#acf8ac215c039be859a7de36e703850e6", null ],
      [ "externInterupt", "Lab__9__main_8py.html#a66ee325f82216f3056d5dbb4a9c930ef", null ],
      [ "externInterupt_button", "Lab__9__main_8py.html#ab808004ddd3bbc8d6895a76e17eb403a", null ],
      [ "faultDetected", "Lab__9__main_8py.html#a9926ec6446fd55516e71cb99fed1101d", null ],
      [ "i2c", "Lab__9__main_8py.html#a86b9a45e5b3f8bdbb43e2f4420c95943", null ],
      [ "length", "Lab__9__main_8py.html#a4846b9409117843fd42af1dd75150c17", null ],
      [ "MotorPIN1", "Lab__9__main_8py.html#adb3b1af17153e5346f60152699941103", null ],
      [ "MotorPIN2", "Lab__9__main_8py.html#a0c2a9063d26f678516dabc5e8a634893", null ],
      [ "MotorPIN3", "Lab__9__main_8py.html#abaa2dac96befb44db23dd6dfb64aed14", null ],
      [ "MotorPIN4", "Lab__9__main_8py.html#a2558defb1cbe0b3061fc6593bcd55602", null ],
      [ "motors", "Lab__9__main_8py.html#a2828581f95771f378e476c131b729546", null ],
      [ "nFault", "Lab__9__main_8py.html#a817a70bf26555724819524f65149a271", null ],
      [ "nSLEEP", "Lab__9__main_8py.html#a8a83f719904820fa4f60575051296b1d", null ],
      [ "timer3", "Lab__9__main_8py.html#af6bf154236b7c0aff1c3c757f42902e4", null ],
      [ "timer4", "Lab__9__main_8py.html#a2b8676bb0303a0c96b4d1a5ed1f30a6b", null ],
      [ "timer8", "Lab__9__main_8py.html#a9efdf826984d7dfabd448571643720e4", null ],
      [ "touch", "Lab__9__main_8py.html#a13d63fb646c861b21d4a0c1294ad3ca5", null ],
      [ "touchXM", "Lab__9__main_8py.html#a9ca163b79d8a21e0788faada2f2fe6af", null ],
      [ "touchXP", "Lab__9__main_8py.html#a9da8f4a86f91c4930452c69dd09a572f", null ],
      [ "touchYM", "Lab__9__main_8py.html#a63403780a98893459b1d863758a62d41", null ],
      [ "touchYP", "Lab__9__main_8py.html#a2631e77960a84097cd26170c8aa072dc", null ],
      [ "width", "Lab__9__main_8py.html#ae66f950c659fbfb196c1a5668b12ca3f", null ]
    ] ],
    [ "main_lab7", null, [
      [ "main", "main__lab7_8py.html#a4e85bef590f998565662b3453ba9a6c9", null ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "MeasureTime", "namespaceMeasureTime.html", [
      [ "NucleoClock", "namespaceMeasureTime.html#abf6f26acdf02efd7e7cb0ff2f9c67a2a", null ],
      [ "p", "namespaceMeasureTime.html#ad49dc22254fbe7f02a254e62df516b44", null ],
      [ "pscale", "namespaceMeasureTime.html#a5eedf23d605bfaade7c4e45548a3c9fe", null ],
      [ "sleepTime", "namespaceMeasureTime.html#a35cdcd8f4e3a5db4422ef04fa0f5c173", null ],
      [ "timer2", "namespaceMeasureTime.html#a0e73119fbb95b5de72825c0a76711b8e", null ],
      [ "TimerClock", "namespaceMeasureTime.html#a914b46d5ab3e44f763a91c0cbd28c9ca", null ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ],
      [ "IN1", "MotorDriver_8py.html#a8c9da946115ac1fd5f771e242016fce6", null ],
      [ "IN2", "MotorDriver_8py.html#a8a0d921223036fd65fdaea12473c08ec", null ],
      [ "IN3", "MotorDriver_8py.html#a0b30d4934d889e0c9b18a300e735dde7", null ],
      [ "IN4", "MotorDriver_8py.html#a7ff8d15dc46254c444cd6f70d72c9495", null ],
      [ "md", "MotorDriver_8py.html#a652821863ea9a99c91c75d2f785fc7d9", null ],
      [ "nSLEEP", "MotorDriver_8py.html#a844049d370e2494076878e17c5bad6f7", null ],
      [ "tim", "MotorDriver_8py.html#a9b2533ecbaedbfb697aeaa799a28eb8b", null ]
    ] ],
    [ "my_script", null, [
      [ "message", "my__script_8py.html#af032d038496b633223a23a7b2f90f420", null ]
    ] ],
    [ "SerialCommunication", null, [
      [ "convertADC", "SerialCommunication_8py.html#a3767bc3c26550496f4c52f6c50a944aa", null ],
      [ "main", "SerialCommunication_8py.html#a0612dac0de9a536b4ea4740d201964c0", null ],
      [ "sendChar", "SerialCommunication_8py.html#a3c53fed4795a5ace6a4567669f39bc4d", null ],
      [ "setupSerialCom", "SerialCommunication_8py.html#acac2da053443070150083500e39aa957", null ]
    ] ],
    [ "temp_main", null, [
      [ "address", "temp__main_8py.html#a58d66fa854035410929a77800614f98a", null ],
      [ "i2c", "temp__main_8py.html#a344c6c9940e849c66b7076ba10793491", null ],
      [ "t", "temp__main_8py.html#a39df22360aa6f2c4e53b94c472d132ec", null ]
    ] ],
    [ "TouchPanel", null, [
      [ "TouchPanel", "classTouchPanel_1_1TouchPanel.html", "classTouchPanel_1_1TouchPanel" ]
    ] ],
    [ "VendingMachine", "namespaceVendingMachine.html", [
      [ "getChange", "namespaceVendingMachine.html#ab7354f19d27c3ab6cd5f54785cf83f90", null ],
      [ "getDrink", "namespaceVendingMachine.html#a49cab5553c894a4c74b1eb3bc783cb11", null ],
      [ "hasInserted", "namespaceVendingMachine.html#a20685f9ba2a563aacdf524c401de8448", null ],
      [ "keyPressed", "namespaceVendingMachine.html#a3db29ebf3c46edfac1e6416374b5f50e", null ],
      [ "printWelcome", "namespaceVendingMachine.html#abfc71baf7cfab5f018c168d44a2d92fb", null ],
      [ "change", "namespaceVendingMachine.html#acef507279ac164766fb70365ace7f984", null ],
      [ "changeTup", "namespaceVendingMachine.html#a70ac196129fe7ad7891d97517b3bb31c", null ],
      [ "eject", "namespaceVendingMachine.html#aa3e52a3cc1b5023d3157932cc4e6bfcd", null ],
      [ "funds", "namespaceVendingMachine.html#aab39abe2e0cfea2f291f9b4891842497", null ],
      [ "payment", "namespaceVendingMachine.html#a5fdcab9affdd6c81f106023426277f01", null ],
      [ "price", "namespaceVendingMachine.html#add9139960dac8c6edf6ec0180cff2312", null ],
      [ "state", "namespaceVendingMachine.html#a24d00013189f65d7975c3060bcae6cd9", null ],
      [ "sufficeint_funds", "namespaceVendingMachine.html#a368084ef1c930fe7976be200e7702f7c", null ]
    ] ]
];